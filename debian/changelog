pymca (5.9.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.2 (routine-update)
  * d/p/0002-fix-data-installation.patch

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 03 Mar 2025 16:33:31 +0100

pymca (5.9.3+dfsg-2) unstable; urgency=medium

  * Team upload
  * Add dependency on python3-pyqt5.qtsvg to python3-pymca5, as
    python3-qtconsole requires this (closes: #1093477)

 -- Julian Gilbey <jdg@debian.org>  Mon, 03 Feb 2025 06:45:39 +0000

pymca (5.9.3+dfsg-1) unstable; urgency=medium

  * d/watch: support PyMca -> [Pp]y[Mm]ca in order to deal with new name
  * New upstream version 5.9.3+dfsg
  * d/t/gui: updated to be more resilient and easyer to read.
  * d/rules: Install also the appstream meta info files

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 05 Aug 2024 16:07:38 +0200

pymca (5.9.2+dfsg-2) unstable; urgency=medium

  * d/p/0002-Replace-assert_array_equal-by-assert_allclose-in-tes.patch:
    Useless now.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 05 Mar 2024 15:00:30 +0100

pymca (5.9.2+dfsg-1) unstable; urgency=medium

  * New upstream version 5.9.2+dfsg
  * use LIBGL_ALWAYS_INDIRECT=1 in order to avoid issue with OpenGL during
    the tests.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 04 Mar 2024 16:12:16 +0100

pymca (5.8.7+dfsg-2) unstable; urgency=medium

  * d/rules: Forced python3 shebang
  * d/t/control: set a writable HOME

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 20 Jul 2023 12:35:42 +0200

pymca (5.8.7+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Update standards version to 4.6.2, no changes needed.
  * d/control: Added B-D pybuild-plugin-pyproject
  * d/p/numpy-1.24: Removed upstream patches part of this upstream version.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 25 Jun 2023 10:00:58 +0200

pymca (5.8.0+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Add gbp.conf to use pristine-tar & --source-only-changes by default.
  * Add upstream patch to fix test failure with Numpy 1.24.
    (closes: #1027240, #1029242)

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 21 Jan 2023 21:52:14 +0400

pymca (5.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.8.0+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 06 Dec 2022 15:46:56 +0100

pymca (5.7.2+dfsg-1) unstable; urgency=medium

  [ Julian Gilbey ]
  * Revert unnecessary change in autopkgtest test
  * Depend on locales-all for autopkgtest; PyMca5/tests/SpecfileTest.py
    attempts to switch between locales

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.7.2+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 04 Jun 2022 10:15:01 +0200

pymca (5.7.1+dfsg-2) unstable; urgency=medium

  * Team upload
  * Relax equality tests in test suite to avoid spurious floating point
    comparison failures (closes: #1010258)
  * Specify LC_ALL and HOME in autopkgtest test

 -- Julian Gilbey <jdg@debian.org>  Wed, 27 Apr 2022 22:04:48 +0100

pymca (5.7.1+dfsg-1) unstable; urgency=medium

  * New upstream version 5.7.1+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 18 Jan 2022 17:07:47 +0100

pymca (5.7.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.7.0+dfsg
  * d/p/*: refreshed the patch series
  * d/control: Removed B-D libglu1-mesa-dev and libqhull-dev
  * d/copyright:
    * No more third-party
    * Updated for the new version
    * Remove unused license definitions for LGPL-2+.
  * Update standards version to 4.6.0, no changes needed.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 18 Jan 2022 11:30:06 +0100

pymca (5.6.7+dfsg-2) unstable; urgency=medium

  * d/p/0003-fix-for-python3.10.patch: Added to fix autopkgtest.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 23 Dec 2021 11:51:51 +0100

pymca (5.6.7+dfsg-1) unstable; urgency=medium

  [ Neil Williams ]
  * Update control to add Debian PaN maintainers

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.6.7+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 22 Dec 2021 16:00:44 +0100

pymca (5.6.5+dfsg-2) unstable; urgency=medium

  * Bug fix: "Removal of the python3-*-dbg packages in sid/bookworm",
    thanks to Matthias Klose (Closes: #994333).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 19 Sep 2021 10:55:12 +0200

pymca (5.6.5+dfsg-1) unstable; urgency=medium

  * New upstream version 5.6.5+dfsg
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Apply multi-arch hints.
    + python3-pymca5-dbg: Add Multi-Arch: same.
  * d/p/0002-switch-to-qhull_r.patch: Added
  * Bug fix: "depends on deprecated qhull library", thanks to Timo Röhling
    (Closes: #990396).
  * d/control: Used dh-sequence-[numpy3,python3,sphinxdoc]
  * d/rules:
    + Simplifier thanks to dh-sequence-X
    + Used execute_[after|before]

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 05 Sep 2021 12:14:22 +0200

pymca (5.6.3+dfsg-1) unstable; urgency=medium

  * New upstream version 5.6.3+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 06 Jan 2021 15:15:17 +0100

pymca (5.6.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.6.0+dfsg
  * d/control: Bumped Standards-Version to 4.5.0 (nothing to do)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 08 Oct 2020 15:05:31 +0200

pymca (5.5.5+dfsg-2) unstable; urgency=medium

  * prepare for the qhull transition (Closes: #956432)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 25 Apr 2020 17:24:32 +0200

pymca (5.5.5+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.5+dfsg
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 18 Apr 2020 18:09:17 +0200

pymca (5.5.4+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.4+dfsg

 -- Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>  Thu, 30 Jan 2020 11:37:55 +0200

pymca (5.5.3+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.3+dfsg

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 24 Oct 2019 22:08:12 +0200

pymca (5.5.2+dfsg-3) unstable; urgency=medium

  * Remove also the Python2 autopkgtests.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 10 Oct 2019 12:58:15 +0200

pymca (5.5.2+dfsg-2) unstable; urgency=medium

  * Removed Python2 module (Closes: #937478)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 10 Oct 2019 11:38:26 +0200

pymca (5.5.2+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.2+dfsg
  * d/control:
    - Move silx to Recommends (Closes: #941002)
    - Added Rules-Requires-Root: no
  * d/patches:
    - Removed fix_numpy_tests_0ed4a39.patch (applyed upstream)
    - Removed 0002-silx-is-an-optional-dependency.patch (requested by upstream)
    - Added 0002-fix-syntax-error.patch
  * d/rules:
    - Activated hardening

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 06 Oct 2019 11:23:23 +0200

pymca (5.5.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * drop debian patch testInteractionSilxMpl_skip_OpenGL.patch.
    Use of OpenGL in testInteractionSilxMpl is fixed in 5.5.1.
  * drop Build-Depends: python[3]-pyqtgraph
    (the PyQtGraph backend was removed in 5.5.1)
  * debian patch fix_numpy_tests_0ed4a39.patch applies upstream commit
    0ed4a39 to enable tests to pass with numpy 1.16.2. Closes: #935454.

 -- Drew Parsons <dparsons@debian.org>  Thu, 12 Sep 2019 23:46:41 +0800

pymca (5.5.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 5.5.1+dfsg
  * set debhelper compatibility level 12
    - update doc-base file for docs now placed in
      /usr/share/doc/pymca
  * Build-Depends: dbus. Needed to run build-time tests in some chroots.

 -- Drew Parsons <dparsons@debian.org>  Tue, 10 Sep 2019 16:50:53 +0800

pymca (5.5.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * debian patch testInteractionSilxMpl_skip_OpenGL.patch skips
    testInteractionSilxMpl unless OpenGL is available
  * skip OpenGL tests on armhf (OpenGL is not available there)
    Closes: #939589.
  * Standards-Version: 4.4.0
  * mark pymca-data, pymca-doc as Multi-Arch: foreign

 -- Drew Parsons <dparsons@debian.org>  Sat, 07 Sep 2019 02:19:10 +0800

pymca (5.5.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.0+dfsg
  * Bump Standars-Version to (4.3.0) (nothing to do)
  * Migrate the Salsa-CI files to the new framework.
  * d/control
    - unversionned dependency on python[3]-silx[-dbg] (cme)
    - Added xauth, xvfb for the tests.
  * d/patches
    - Added 0002-silx-is-an-optional-dependency.patch
  * Use debhelper-compat instead of debian/compat.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 19 Jul 2019 18:15:51 +0200

pymca (5.4.3+dfsg-1) unstable; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * update the Salsa CI

  [ Alexandre Marie ]
  * New upstream version 5.4.3+dfsg

 -- Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>  Wed, 09 Jan 2019 15:46:16 +0100

pymca (5.4.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.4.0+dfsg
  * d/control
    + python[3]-fisx[-dbg] (>= 1.1.4 -> 1.1.6)
  * d/copyright Updated with upstream informations.
  * d/tests
    - Moves all tests inside control.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 20 Sep 2018 10:05:58 +0200

pymca (5.3.2+dfsg-1) unstable; urgency=medium

  [ Teemu Ikonen ]
  * Remove myself from Uploaders (Closes: #892618)

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.3.2+dfsg
  * Bump Standars-Version to (4.1.4) (nothing to do)
  * Switched pymca scripts to Python3
  * d/control
    - Updated VCS-x for salsa migration.
    - Build-Depends:
      + python[3]-fisx[-dbg] <!nocheck> <!nodoc> (Added)
      + python[3]-h5py[-dbg] <!nocheck> <!nodoc> (Added)
      + python[3]-ipython (Added)
      + python[3]-matplotlib[-dbg]  <!nodoc> (Added)
      + python[3]-opengl <!nodoc>,
      + python[3]-pyqt5[-dbg] <!nodoc>,
      + python[3]-pyqt5.qtopengl[-dbg] <!nodoc>,
      + python[3]-pyqtgraph,
      + python[3]-qtconsole,
      + python[3]-silx,
  * d/patches
    - 0001-xlocale.h-not-available-under-glibc-2.26.patch (obsolete)
    + 0002-use-the-local-mathjax.patch (Added)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 03 Aug 2018 11:48:52 +0200

pymca (5.2.2+dfsg-2) unstable; urgency=medium

  * fix FTBFS with glibc2.26 (Closes: #887749)
  * d/patches/
    - 0001-xlocale.h-not-available-under-glibc-2.26.patch (Added)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 20 Jan 2018 15:26:30 +0100

pymca (5.2.2+dfsg-1) unstable; urgency=medium

  * New upstream version 5.2.2+dfsg
  * Bump Standars-Version to (4.1.1) (extra -> optional)
  * d/watch use the pypi redirector
  * d/control
    - Build-Depends: fisx (>= 1.1.4-1~)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 21 Oct 2017 12:26:49 +0200

pymca (5.1.3+dfsg-1) unstable; urgency=medium

  * New upstream version 5.1.3+dfsg
  * Depends fisx >= 1.1.2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 27 Nov 2016 12:17:50 +0100

pymca (5.1.2+dfsg-2) unstable; urgency=medium

  * Added Breaks and Replace to fix the jessie -> strech upgrade (Closes:
    #836850)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 11 Sep 2016 09:24:04 +0200

pymca (5.1.2+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 5.1.2+dfsg
  * switch to compat level 9
  * Add the autopkgtest suite
  * Use cython to regenerate the extensions
  * debian/control
    + Homepage: https://github.com/vasole/pymca
    + new binary packages:
      - python-pymca5, python-pymca5-dbg
      - python3-pymca5, python3-pymca5-dbg
      - pymca-doc
    + Update Vcs-git
    + bump Standards-Version 3.9.8 (nothing to do)
    + Build-Depends Added:
      - cython, cython3
      - python-setuptools, python3-setuptools
  * debian/copyright
    + Use Files-Excluded to remove dfsg-nonfree files

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 20 Aug 2016 12:42:18 +0200

pymca (4.7.4+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 4.7.4+dfsg
  * debian/patches
    - 0003-forwaded-upstream-allow-to-build-with-the-system-qhu.patch
      (an equivalent patch was applyed by upstream)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 01 Oct 2014 21:38:53 +0200

pymca (4.7.1+dfsg-2) unstable; urgency=medium

  * debian/control
    update the description: pymca support also hdf5 files
  * debian/patches
    - 0002-debian-fix-header-for-system-qhull.patch
      do not apply to fix the FTBFS (Closes: #738752)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 16 Feb 2014 11:54:36 +0100

pymca (4.7.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 4.7.1+dfsg
  * Repack to remove the embeded qhull library
  * debian/control
    - Build-Depends: add libqhull-dev
  * debian/patches
    + 0002-debian-fix-header-for-system-qhull.patch
    + 0003-forwaded-upstream-allow-to-build-with-the-system-qhu.patch

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 14 Dec 2013 12:40:37 +0100

pymca (4.7.0+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 4.7.0+dfsg
  * Repack to remove the PyMca.sift module
  * Bump Standars-Version to (3.9.5) nothing to do

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 07 Dec 2013 10:13:57 +0100

pymca (4.6.2-1) unstable; urgency=low

  * Imported Upstream version 4.6.2
  * Bump Standars-Version to (3.9.4) nothing to do
  * debian/control
    - unversionne python-all-dev (cme fix)
  * debian/copyright updated
  * debian/patches
    - 0002-fix-from-upstream-hurd-FTBFS.patch (deleted)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 01 Jun 2013 14:41:59 +0200

pymca (4.6.0-2) unstable; urgency=low

  * debian/patches
    - 0002-fix-from-upstream-hurd-FTBFS.patch (new)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 19 Jun 2012 18:10:01 +0200

pymca (4.6.0-1) unstable; urgency=low

  * Imported Upstream version 4.6.0
  * debian/patches
    - 0001-Hardcode-correct-Debian-paths-to-executable-scripts.patch (renamed)
      0001-install-the-data-at-the-right-place.patch
    - 0002-Various-build-system-fixes.patch (deleted)
    - 0003-feature-forwarded-missing-EADL97_KShellConstants.dat.patch (deleted)
  * debian/rules
    - execute the unit tests just for information now.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 18 Jun 2012 21:18:07 +0200

pymca (4.5.0-4) unstable; urgency=low

  * debian/rules
    - fix the permission to avoid FTBFS
      sbuild and pbuilder do not failed the same way.
    - remove the PyMca/HTML from python modules (documentation)
  * debian/patches
    - 0003-feature-forwarded-missing-EADL97_KShellConstants.dat.patch (new)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 19 Mar 2012 09:24:06 +0100

pymca (4.5.0-3) unstable; urgency=low

  * fix permission to avoid FTBFS

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 19 Mar 2012 08:06:44 +0100

pymca (4.5.0-2) unstable; urgency=low

  * oups FTBFS everywhere

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 18 Mar 2012 23:13:46 +0100

pymca (4.5.0-1) unstable; urgency=low

  * Imported Upstream version 4.5.0 (Closes: #656198)
  * debian/control
    - Upgrade to standards-version 3.9.3 (nothing to do)
    - Remove python-support
    - Add myself as Uploaders
  * debian/copyright
    - Updated
  * debian/patches
    - Unapply patches from source
    - Refresh the series using gbp-pq
  * debian/rules
    - switch to dh_python2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 17 Mar 2012 21:48:02 +0100

pymca (4.4.1p1-2) unstable; urgency=low

  * control:
    - Move python-matplotlib to Recommends
    - Add python-qt4-gl, python-opengl, python-mdp and python-h5py to
      Recommends (closes: #623757).
  * Perpeatuate the hack of manually deleting files installed to the
    pymca-data package from the pythonX.X installation directories.
    Support the versions of Python in unstable (2.6 and 2.7) and drop
    the support for python2.5. This means that this revision of pymca
    does not build any more in squeeze. Upstream has promised to tweak
    the setup.py to better support splitting of data files in the next
    release (closes: #625171).

 -- Teemu Ikonen <tpikonen@gmail.com>  Mon, 23 May 2011 16:15:48 +0200

pymca (4.4.1p1-1) unstable; urgency=low

  * New upstream version.
    - Fixes setting PYTHONPATH in scripts (closes: #605160).
    - Includes the specfile module locale fixes, so removing patch
      03_specfile-locale.
    - Does not include .pyc files in the orig.tar, so removing code
      in 'debian/rules' for preserving them during the build.
  * Patch 03_postbatch: Remove starting blank line from pymcapostbatch
    script to allow correct distutils #! replacement.

 -- Teemu Ikonen <tpikonen@gmail.com>  Tue, 30 Nov 2010 12:32:47 +0100

pymca (4.4.0-2) unstable; urgency=low

  * Add patch from upstream to fix reading of specfiles (closes: #602471)
  * Add debian/source/format file with contents '3.0 (quilt)'
  * Add a README.source file.
  * Split changes to upstream sources to patches:
    - 01_paths.patch: Hardcode Debian specific documentation and Python paths.
    - 02_setup.patch: Misc. changes to setup.py. Do not show license during
      build, do not install docs to Python dirs, do not remove old files.
    - 03_specfile-locale.patch: Read specfiles with correct 'C' locale.
      Define SPECFILE_USE_GNU_SOURCE in setup.py to enable the fix.
  * Add debian/metapatches/* (DEP3 header info for patches generated from git).
  * copyright: Fix typo, "algoritm" -> "algorithm"

 -- Teemu Ikonen <tpikonen@gmail.com>  Thu, 18 Nov 2010 10:58:07 +0100

pymca (4.4.0-1) unstable; urgency=low

  * New upstream version.
  * Upload to unstable (Closes: #514903)
  * setup.py: Use lib-path based on build-time interpreter version
    in scripts.
  * control:
    - Split the data files to arch-independent package pymca-data. Make
      pymca depend on it.
    - Upgrade to standards-version 3.9.1.
    - Upgrade debhelper build-dep to version >= 7.0.50~ because of the
      use of overrides in rules.
    - Add build-dep to libglu1-mesa-dev | libglu-dev.
  * rules:
    - Preserve phynx .pyc files during clean.
    - Use overrides, delete data files from the pymca package.
  * copyright:
    - Upgrade format specification to dep5.
    - Add copyright information for nnma and phynx.

 -- Teemu Ikonen <tpikonen@gmail.com>  Fri, 03 Sep 2010 14:20:02 +0200

pymca (4.3.0-1) UNRELEASED; urgency=low

  * Initial release.

 -- Teemu Ikonen <tpikonen@gmail.com>  Mon, 16 Feb 2009 15:42:16 +0100
